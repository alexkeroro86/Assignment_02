# Software Studio 2018 Spring Assignment 02

## 小朋友下樓梯
<img src="assets/example01.png" width="660px" height="525px"></img>
<img src="assets/example13.png" width="660px" height="525px"></img>

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Report
#### 1. Stage Manager
* 分別為boot, load, menu, record, play, 和over六個處理階段
    * boot: 預先載入load會用到的素材和google font
    * load: 預先載入之後會用到的所有素材，包含圖片和音效，有動態的讀取條和文字表示進度
    * menu: 控制遊戲選項，包含難易度、多人遊玩、開關音效和進入record，為開始遊戲前的預備狀態
    * record: 根據難易度區別的排行榜，且分別由最高積分的前五名排序
    * play: 遊戲主體，除此之外包含放棄和暫停
    * over: 根據單人或多人而有不同介面，為遊戲結束的統整以及上傳資料
* 若有跨stage的資料則用全域變數存取

#### 2. Basic Rules
* 共有六種平台會出現在遊戲中，且各有各的屬性與玩家產生不同效果，而每位玩家一開始有五次失誤的機會
* 單人模式
    * 有三種會扣血的平台和天花板，每次碰到時會產生1.2秒的超級狀態不會被扣血，當扣到零時則結束遊戲
    * 有兩種無害的平台，其中有一種當玩家血低於五時有補血的效果
    * 最後有兩種會影響玩家移動的平台，可以使用奔跑來加速逃脫，但是根據使用時間會扣積分，當積分為零時則不可使用
    * 若玩家掉落出地圖，則直接結束遊戲
    * 各個平台的出現機率依據選擇的難度而有所不同
* 多人模式
    * 平台的種類同於單人模式
    * 勝利的條件則是改為當其中一方出局時結束
    * 在此模式中並無奔跑的功能

#### 3. Physical Behaviors
* 分為玩家與平台和玩家與邊界的碰撞
    * 前者為平台只會持續向上升，向右和向左的平台會使玩家直接產生相對位移，而有敵人的平台只有玩家站在上面時才會有反應，且所有平台只會在上方有物理效果
    * 後者如同碰觸慣性極大的物體，因此只有玩家會被阻擋，如左右邊的牆壁，天花板則是用重疊感應來扣血
* 玩家被持續相同重力影響，至於在空中時只會有固定的平移速度，除非使用奔跑產生更遠的跳躍
* 因為圖片大小來當作碰撞的四邊形，所以根據圖片的留白處來偏移和更改碰撞的大小

#### 4. Traps
* 普通的平台: 無其他特殊功能
    * ![normal platform](assets/example02.png)
* 向右輸送的平台: 使玩家產生向右位移
    * ![right platform](assets/example03.png)
* 向左輸送的平台: 使玩家產生向左位移
    * ![left platform](assets/example04.png)
* 有尖刺的平台: 碰到玩家會扣血
    * ![nail platform](assets/example05.png)
* 有敵人的平台: 有敵人在平台間隨機產生，視為不可移動的障礙物，觸碰亦會扣血
    * ![enemy platform](assets/example06.png)
* 碰到會移動的平台: 當玩家站上去時會隨機決定往左或右的方向移動，但是當玩家血量低於五時會補充一次血量
    * ![up platform](assets/example07.png)

#### 5. Sound Effects and UI
* 音效
    * 在開始遊戲、進入排行榜、放棄遊戲、遊戲結束和破紀錄時會有音效
    * 當玩家被扣血或是補血時也有相對應的音效
    * 所有音效可在menu時選取靜音
* 使用者介面
    * menu: 共有三種不同形式的按鈕，其一是鼠標移至上方會有動畫，為選擇多人遊玩，會根據模式在左方顯示對應人數，在按一次回到單人，其二是根據正負狀態切換透明度，為聲音的開關，其三是內建按鈕作單張切換，會跳到排行榜，最後和用按鍵觸發
        * ![2p btn](assets/example08.png)
        * ![mute btn](assets/mute.png)
        * ![record btn](assets/example09.png)
    * record: 點擊文字產生按鈕的效果
    * play: 右下方有兩個動畫式的按鈕，分別為放棄和暫停
        * ![abort btn](assets/example10.png)
        * ![stop btn](assets/example11.png)
    * over: 鍵盤輸入的事件處理

#### 6. Firebase Real-time Database
* 根據難易度使用不同的區塊存取，上傳的內容包含玩家姓名和積分
    * ![database](assets/example12.png)
* 在不輸入姓名時會阻止上傳

#### 7. Appearance
* 有很多動畫，包含每種玩家、平台、血、按鈕和文字
* 會根據不同的難易度而有不同的背景
* 整體遊戲的文字用googl font
* 讀取條的動畫使用兩層，內層才是會變動的，因為讀比較慢可以看清楚這部分的動畫

#### 8. Other Features
* 多人模式
    * Player1 ![player1](assets/example15.png)
    * Player2 ![player2](assets/example14.png)
* 在遊戲中可以暫停或放棄
* 可以直接用鍵盤輸入姓名，且能切換大小寫
* 增加奔跑功能
* 可以切換難易度和靜音模式
* 有積分評比機制和刷新紀錄的音效
* 有遊戲結束的動畫
* 輸入姓名的功能
* 角色是站在魔毯中間，有俯視的感覺，有用moveUp的方式改變渲染的次序
* 地圖切換的過場動畫
* 有超級狀態並搭配閃爍
* 多人模式的扣血方向是往兩邊減少