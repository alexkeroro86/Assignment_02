/*jshint esversion:6*/

var loadState = {
    preload: function() {
        game.stage.backgroundColor = '#123456';

        var progressLabel = game.add.text(game.width * 0.5, game.height * 0.5 - 50, 'Loading...');
        progressLabel.anchor.setTo(0.5, 0.5);
        progressLabel.font = 'Fredoka One';
        progressLabel.fontSize = 50;
        progressLabel.fill = '#fcfcfc';

        var progressBarContent = game.add.sprite(game.width * 0.5 - 300, game.height * 0.5, 'progressBarContent');
        var progressBarBorder = game.add.sprite(game.width * 0.5 - 300, game.height * 0.5, 'progressBarBorder');
        game.load.setPreloadSprite(progressBarContent);

        game.load.spritesheet('player', './assets/player.png', 45, 45);
        game.load.spritesheet('player2', './assets/player2.png', 45, 50);
        game.load.spritesheet('enemy1', './assets/enemy1.png', 31, 39);
        game.load.spritesheet('platformNormal', './assets/platformNormal.png', 80, 25);
        game.load.spritesheet('platformUp', './assets/platformUp.png', 80, 25);
        game.load.spritesheet('platformRight', './assets/platformRight.png', 98, 16);
        game.load.spritesheet('platformLeft', './assets/platformLeft.png', 98, 16);
        game.load.spritesheet('platformNail', './assets/platformNail.png', 97, 30);
        game.load.spritesheet('platformEnemy', './assets/platformEnemy.png', 100, 25);
        game.load.spritesheet('borderTop', './assets/borderTop.png', 500, 75);
        game.load.image('borderLeft', './assets/borderLeft.png');
        game.load.image('borderRight', './assets/borderRight.png');
        game.load.image('background', './assets/background.png');
        game.load.image('bg0', './assets/bg0.png');
        game.load.image('bg1', './assets/bg1.png');
        game.load.spritesheet('hp', './assets/hp.png', 40, 35);
        game.load.spritesheet('hp2', './assets/hp2.png', 40, 35);

        game.load.audio('dead', './sounds/dead.wav');
        game.load.audio('hit', './sounds/hit.wav');
        game.load.audio('start', './sounds/start.wav');
        game.load.audio('restore', './sounds/restore.wav');
        game.load.audio('abort', './sounds/abort.wav');
        game.load.audio('record', './sounds/record.wav');
        game.load.audio('high', './sounds/high.wav');

        game.load.spritesheet('abortBtn', './assets/abortBtn.png', 96, 90);
        game.load.spritesheet('stopBtn', './assets/stopBtn.png', 85, 100);
        game.load.image('muteBtn', './assets/mute.png');
        game.load.spritesheet('multiBtn', './assets/multiBtn.png', 196, 100);
        game.load.spritesheet('score', './assets/score.png', 160, 172);
        game.load.spritesheet('leaderboardBtn', './assets/leaderboard.png', 150, 180);
        game.load.image('background2', './assets/background2.png');
    },
    create: function() {
        game.state.start('menu');
    },
    update: function() {},
};