/*jshint esversion:6*/

var recordState = {
    preload: function() {
        game.add.image(0, 0, 'background2');
    },
    create: function() {
        var records0 = [];
        var records1 = [];

        this.textRecord = game.add.text(game.width * 0.35, game.height * 0.15, `Leaderboard`);
        this.textRecord.anchor.setTo(0.5, 0.5);
        this.textRecord.font = 'Fredoka One';
        this.textRecord.fontSize = 80;
        let grd = this.textRecord.context.createLinearGradient(0, 0, 0, this.textRecord.canvas.height);
        grd.addColorStop(0, '#91ffe1');
        grd.addColorStop(1, '#0cad81');
        this.textRecord.fill = grd;

        this.backBtn = game.add.text(game.width * 0.85, game.height * 0.9, 'BACK');
        this.backBtn.stroke = "#ffdd70";
        this.backBtn.strokeThickness = 15;
        this.backBtn.anchor.setTo(0.5, 0.5);
        this.backBtn.font = 'Fredoka One';
        this.backBtn.fontSize = 50;
        grd = this.backBtn.context.createLinearGradient(0, 0, 0, this.backBtn.canvas.height);
        grd.addColorStop(0, '#ffd344');   
        grd.addColorStop(1, '#bf9200');
        this.backBtn.fill = grd;
        this.backBtn.inputEnabled = true;
        this.backBtn.input.useHandCursor = true;
        this.backBtn.events.onInputOver.add(() => {
            this.backBtn.stroke = "#fcee6f";
        }, this);
        this.backBtn.events.onInputOut.add(() => {
            this.backBtn.stroke = "#ffdd70";
        }, this);
        this.backBtn.events.onInputDown.add(() => {
            this.backBtn.stroke = "#d1bc00";
            game.sound.stopAll();
            game.state.start('menu');
        }, this);

        let mode = game.add.text(game.width * 0.2, 120 + game.height * 0.1, `Normal`);
        mode.font = 'Fredoka One';
        mode.fontSize = 25;
        mode.fill = '#004CB3';
        mode = game.add.text(game.width * 0.5, 120 + game.height * 0.1, `Hard`);
        mode.font = 'Fredoka One';
        mode.fontSize = 25;
        mode.fill = '#c90202';

        firebase.database().ref(`m_leaderboard0`).orderByChild('score').limitToLast(5).once('value').then(snapshot => {
            snapshot.forEach(childSnapshot => {
                let childData = childSnapshot.val();
                records0.unshift(childData);
            });
            if(game.state.current == 'record') {
                for(let i = 0; i < records0.length; ++i) {
                    let recordIdx = i + 1;
                    let rules = game.add.text(game.width * 0.2, 150 + game.height * 0.1 * recordIdx, `${recordIdx} ${recordIdx == 1 ? 'st' : recordIdx == 2 ? 'nd' : recordIdx == 3 ? 'rd' : 'th'} ${records0[i].name}: ${records0[i].score}\n`);
                    rules.font = 'Fredoka One';
                    rules.fontSize = 25;
                    rules.fill = '#000';
                }
            }
        }).catch(err => { console.log(err.message); });

        firebase.database().ref(`m_leaderboard1`).orderByChild('score').limitToLast(5).once('value').then(snapshot => {
            snapshot.forEach(childSnapshot => {
                let childData = childSnapshot.val();
                records1.unshift(childData);
            });
            if(game.state.current == 'record') {
                for(let i = 0; i < records1.length; ++i) {
                    let recordIdx = i + 1;
                    let rules = game.add.text(game.width * 0.5, 150 + game.height * 0.1 * recordIdx, `${recordIdx} ${recordIdx == 1 ? 'st' : recordIdx == 2 ? 'nd' : recordIdx == 3 ? 'rd' : 'th'} ${records1[i].name}: ${records1[i].score}\n`);
                    rules.font = 'Fredoka One';
                    rules.fontSize = 25;
                    rules.fill = '#000';
                }
            }
        }).catch(err => { console.log(err.message); });
    },
    update: function() {},
};