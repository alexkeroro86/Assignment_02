/*jshint esversion:6*/

var bootState = {
    preload: function() {
        game.load.image('progressBarBorder', './assets/progressBarBorder.png');
        game.load.image('progressBarContent', './assets/progressBarContent.png');

        game.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
    },
    create: function() {
        game.state.start('load');
    },
};