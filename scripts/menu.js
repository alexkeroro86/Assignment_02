/*jshint esversion:6*/

var bgIdx = 0;
var isMultiPlayer = false;

var menuState = {
    preload: function() {},
    create: function() {
        this.keySpace = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.keySpace.onDown.add(this.start, this);

        this.modes = {0: 'normal', 1: 'hard'};
        
        this.background = game.add.sprite(0, 0, 'background');
        this.gameBackground = game.add.image(0, 100, `bg${bgIdx}`);
        this.gameBackground.inputEnabled = true;
        this.gameBackground.events.onInputDown.add(() => {
            if(!game.tweens.isTweening(this.gameBackground)) {
                bgIdx = (bgIdx + 1) % 2;
                this.bgTween = game.add.tween(this.gameBackground).to({alpha: 0.5}, 500).start();
                this.bgTween.onComplete.add( () => {
                    this.modeText.text = `${this.modes[bgIdx]}`;
                    let grd = this.modeText.context.createLinearGradient(0, 0, 0, this.modeText.canvas.height);
                    if(bgIdx == 1) {
                        grd.addColorStop(0, '#ff6060');   
                        grd.addColorStop(1, '#c90202');
                    }
                    else if(bgIdx == 0) {
                        grd.addColorStop(0, '#8ED6FF');   
                        grd.addColorStop(1, '#004CB3');
                    }
                    this.modeText.stroke = (bgIdx == 1) ? "#ffc1d3" : '#c1e3ff';
                    this.modeText.fill = grd;
                    this.gameBackground.loadTexture(`bg${bgIdx}`);
                    game.add.tween(this.gameBackground).to({alpha: 1}, 500).start();
                }, this);
            }
        }, this);
        
        this.modeText = game.add.text(250, 50, `${this.modes[bgIdx]}`);
        this.modeText.stroke = (bgIdx == 1) ? "#ffc1d3" : '#c1e3ff';
        this.modeText.strokeThickness = 10;
        this.modeText.anchor.setTo(0.5, 0.5);
        this.modeText.font = 'Fredoka One';
        this.modeText.fontSize = 40;
        let grd = this.modeText.context.createLinearGradient(0, 0, 0, this.modeText.canvas.height);
        if(bgIdx == 1) {
            grd.addColorStop(0, '#ff6060');   
            grd.addColorStop(1, '#c90202');
        }
        else if(bgIdx == 0) {
            grd.addColorStop(0, '#8ED6FF');   
            grd.addColorStop(1, '#004CB3');
        }
        this.modeText.fill = grd;

        if(isMultiPlayer) {
            this.player = game.add.sprite(220, 400, 'player');
            this.player.anchor.setTo(0.5, 0.5);
            this.player2 = game.add.sprite(280, 400, 'player2');
            this.player2.anchor.setTo(0.5, 0.5);
        }
        else {
            this.player = game.add.sprite(250, 400, 'player');
            this.player.anchor.setTo(0.5, 0.5);
        }

        this.rules = game.add.text(game.width * 0.575, game.height * 0.15, 'Click left image to change mode\nKEY Z + Arrow to run\nArrow Right/(2P)NUM 6 to walk right\nArrow Left/(2P)NUM 4 to walk left\nFollowing are 2p, mute, and reocrd');
        this.rules.font = 'Fredoka One';
        this.rules.fontSize = 20;
        this.rules.fill = '#000';

        this.textStart = game.add.text(game.width * 0.7, game.height * 0.9, 'Press SPACE to start');
        this.textStart.stroke = "#ffdd70";
        this.textStart.strokeThickness = 15;
        this.textStart.anchor.setTo(0.5, 0.5);
        this.textStart.font = 'Fredoka One';
        this.textStart.fontSize = 50;
        grd = this.textStart.context.createLinearGradient(0, 0, 0, this.textStart.canvas.height);
        grd.addColorStop(0, '#ffd344');   
        grd.addColorStop(1, '#bf9200');
        this.textStart.fill = grd;
        game.add.tween(this.textStart).to({alpha: 0.6}, 1000).easing(Phaser.Easing.Sinusoidal.In).yoyo(true).loop().start();

        this.leaderboardBtn = game.add.button(game.width * 0.85, game.height * 0.65, 'leaderboardBtn', () => {
            game.add.audio('record').play();
            game.state.start('record');
        }, this, 1, 0, 2);
        this.leaderboardBtn.anchor.setTo(0.5, 0.5);

        this.muteBtn = game.add.image(game.width * 0.68, game.height * 0.68, 'muteBtn');
        this.muteBtn.anchor.setTo(0.5, 0.5);
        this.muteBtn.alpha = (game.sound.mute) ? 0.5 : 1;
        this.muteBtn.inputEnabled = true;
        this.muteBtn.input.useHandCursor = true;
        this.muteBtn.events.onInputUp.add(() => {
            game.sound.mute = !(game.sound.mute);
            this.muteBtn.alpha = (game.sound.mute) ? 0.5 : 1;
        }, this);

        this.multiBtn = game.add.sprite(game.width * 0.78,  game.height * 0.45, 'multiBtn');
        this.multiBtn.alpha = (isMultiPlayer) ? 1 : 0.5;
        this.multiBtn.animations.add('mouseon', [0, 1], 7, true);
        this.multiBtn.anchor.setTo(0.5, 0.5);
        this.multiBtn.inputEnabled = true;
        this.multiBtn.input.useHandCursor = true;
        this.multiBtn.events.onInputOver.add(() => {
            this.multiBtn.animations.play('mouseon');
        }, this);
        this.multiBtn.events.onInputOut.add(() => {
            this.multiBtn.frame = 0;
            this.multiBtn.animations.stop();
        }, this);
        this.multiBtn.events.onInputUp.add(() => {
            isMultiPlayer = !isMultiPlayer;
            if(isMultiPlayer) {
                this.player.destroy();
                this.player = game.add.sprite(220, 400, 'player');
                this.player.anchor.setTo(0.5, 0.5);
                this.player2 = game.add.sprite(280, 400, 'player2');
                this.player2.anchor.setTo(0.5, 0.5);
            }
            else {
                this.player.destroy();
                this.player2.destroy();
                this.player = game.add.sprite(250, 400, 'player');
                this.player.anchor.setTo(0.5, 0.5);
            }
            this.multiBtn.alpha = (isMultiPlayer) ? 1 : 0.5;
        }, this);
    },
    start: function() {
        game.sound.stopAll();
        game.add.audio('start').play();
        game.state.start('play');
    },
};